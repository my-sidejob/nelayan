<?php

namespace App\Http\Controllers;

use App\Ikan;
use App\Tambak;
use Illuminate\Http\Request;

class ListingController extends Controller
{
    public function ikan()
    {
        $ikan = Ikan::all();
        return view('ikan', compact('ikan'));
    }

    public function tambak()
    {
        $tambak = Tambak::all();
        return view('tambak', compact('tambak'));
    }

    public function detailTambak($id)
    {
        $tambak = Tambak::findOrFail($id);
        $ikan = Ikan::where('tambak_id', $id)->get();
        return view('detail-tambak', compact('tambak', 'ikan'));
    }
}
