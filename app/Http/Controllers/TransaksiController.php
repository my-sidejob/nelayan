<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pembelian;

class TransaksiController extends Controller
{
    public function index()
    {
        $pembelian = Pembelian::orderBy('created_at', 'desc')->get();
        return view('admin.transaksi.index', compact('pembelian'));
    }

    public function detail($transaksi_id)
    {
        $pembelian = Pembelian::find($transaksi_id);
        return view('admin.transaksi.detail', compact('pembelian'));
    }

    public function changeStatus(Request $request, $transaksi_id)
    {
        $pembelian = Pembelian::find($transaksi_id);
        $pembelian->update($request->all());

        return redirect()->back()->with('success', 'Status Pembayaran Berhasil diubah');
    }
}
