<?php

namespace App\Http\Controllers;

use App\Ikan;
use App\Pembelian;
use App\Tambak;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index(){
        $user_id = Auth::user()->id;
        
        if(Auth::user()->level == 1){
            $total_produk = Ikan::all()->count(); 
            $jumlah_tambak = Tambak::all()->count();
            $jumlah_transaksi = Pembelian::all()->count();
            $jumlah_omset = Pembelian::where('status_pembelian', '!=', '1')->sum('total_nominal');
        } else {
            $tambak = Tambak::select('id')->where('user_id', $user_id)->get()->toArray();
            $tambak_id = '';
            foreach($tambak as $list){
                $tambak_id .= $list['id'] . ', ';
            }
            $total_produk = Ikan::whereIn('tambak_id', [$tambak_id])->count();
            $jumlah_tambak = Tambak::where('user_id', $user_id)->count();
            
            $jumlah_transaksi = Pembelian::getJumlahTransaksiNelayan($user_id);
            $jumlah_omset = Pembelian::getJumlahOmsetNelayan($user_id);
        }
        return view('admin.dashboard', compact('total_produk', 'jumlah_tambak', 'jumlah_transaksi', 'jumlah_omset'));
    }
}
