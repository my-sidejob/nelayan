<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    public $table = 'pembelian';
    protected $fillable = [
        'invoice', 'tanggal_pembelian', 'total_nominal','status_pembelian','pelanggan_id'
    ];
    public function pembayaran() {
        return $this->hasOne('App\Pembayaran','pembelian_id','id');
    }

    public function getStatusPembelian()
    {
        if($this->status_pembelian == 1){
            return 'Menunggu Pembayaran';
        }else if ($this->status_pembelian == 2){
            return 'Menunggu Konfirmasi Pembayaran';
        } else if ($this->status_pembelian == 3){
            return 'Sedang Dikirim';
        } else if ($this->status_pembelian == 4){
            return 'Selesai';
        } else {
            return 'Batal';
        }
    }

    public function pelanggan()
    {
        return $this->belongsTo('App\Pelanggan');
    }

    public function detailPembelian()
    {
        return $this->hasMany('App\DetailPembelian');
    }

    public function pengiriman()
    {
        return $this->hasOne('App\Pengiriman');
    }


    public function checkTambak()
    {
        $tambak = $this->detailPembelian()->first();
        if($tambak->ikan == null){

            return "ikan/tambak tidak diketahui atau terhapus.";
        } else {
            return $tambak->ikan->tambak->nama_tambak;
        }
    }

    public function checkTambakOwner()
    {
        $tambak = $this->detailPembelian()->first();
        if($tambak->ikan == null){

            return "ikan/tambak tidak diketahui atau terhapus.";
        } else {
            return $tambak->ikan->tambak->user_id;
        }
    }

    public static function getJumlahTransaksiNelayan($user_id)
    {
        $pembelian = Pembelian::all();
        $count = 0;
        foreach($pembelian as $beli){
            if($beli->checkTambakOwner() != $user_id){
                continue;
            } else {
                $count++;
            }
        }

        return $count;
    }

    public static function getJumlahOmsetNelayan($user_id)
    {
        $pembelian = Pembelian::all();
        $omset = 0;
        foreach($pembelian as $beli){
            if($beli->checkTambakOwner() != $user_id){
                continue;
            } else {
                $omset += $beli->total_nominal;
            }
        }
        return $omset;
    }
}
