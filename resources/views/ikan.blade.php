@extends('template')
@section('title')
Ikan
@endsection
@section('link')

@endsection
@section('content')
<section class="htc__category__area ptb--100">
    <div class="container">
        <div class="htc__product__container">
            <div class="row">
                <div class="product__list clearfix mt--30">
                    <!-- Start Single Category -->
                    @foreach($ikan as $item)
                    <div class="col-md-4 col-lg-3 col-sm-4 col-xs-12">
                        <div class="category">
                            <div class="ht__cat__thumb">
                                <a href="{{route('detail-ikan',$item->id)}}">
                                    <img src="{!!url("storage/foto/".$item->foto)!!}" width="290px" height="290px" alt="product images">
                                </a>
                            </div>
                            
                            <div class="fr__product__inner">
                                <h4><a href="{{route('detail-ikan',$item->id)}}">{{$item->nama_ikan}}</a></h4>
                                <ul class="fr__pro__prize">
                                    
                                    <li>Rp. {{number_format($item->harga)}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- End Single Category -->
                </div>
            </div>
        </div>
    </div>
</section>

@endsection