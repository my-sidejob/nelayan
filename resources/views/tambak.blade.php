@extends('template')
@section('title')
Tambak
@endsection
@section('link')

<style>

.tambak-module {
    border: 1px solid;
    padding: 20px;
    border-radius: 14px;
}

.tambak-thumb img {
    object-fit: cover;
    width: 100%;
    height: 100%;
}
.tambak-thumb {
    width: 194px;
    height: 130px;
}

</style>

@endsection
@section('content')

<section class="htc__category__area ptb--100">
    <div class="container">
        <div class="htc__product__container">
            @foreach($tambak as $row)
            <div class="row" style="margin-bottom:40px;">
                <div class="col-lg-12 col-md-12">
                    <div class="testimonial tambak-module">
                        <div class="testimonial__thumb tambak-thumb">
                            <img src="{{ url('storage/foto/'. $row->foto) }}" alt="testimonial images tambak-img">
                        </div>
                        <div class="testimonial__details ">
                            <h4><a href="{{ url('tambak/'.$row->id) }}" tabindex="0">{{ ucwords($row->nama_tambak) }}</a></h4>
                            <p>{{ $row->no_telp }}</p>
                            <p>{{ $row->alamat }}</p>
                        </div>
                    </div>
                </div>                
            </div>
            @endforeach
            
        </div>
    </div>
</section>

@endsection