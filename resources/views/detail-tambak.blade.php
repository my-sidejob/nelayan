@extends('template')
@section('title')
{{ ucwords($tambak->nama_tambak) }} Detail
@endsection
@section('link')
<style>
.fr__prize__inner h2 {
    padding-right: 20px;
}

.fr__prize__inner h3 {
    font-size: 30px;
}
.tambak-header h4 {
    font-size: 18px;
    font-weight: bold;
}
.tambak-header ul {
    font-size: 18px;
    line-height: 1.7em;
    font-family: "montserrat";
}
.tambak-header hr {
    margin: 1.625em 0;
    border-color: #b5b5b5;
}
</style>
@endsection
@section('content')

<section class="htc__good__sale bg__cat--3">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                <div class="fr__prize__inner tambak-header">
                    <h2>Selamat Datang di Tambak</h2>
                    <h3>{{ ucwords($tambak->nama_tambak) }}</h3>
                    <h4>Detail Tambak</h4>
                    <hr>
                    <ul>
                        <li><strong>Pemilik</strong></li>
                        <li>{{ $tambak->user->nama }}</li>
                        <li><strong>No. Telp</strong></li>
                        <li>{{ $tambak->no_telp }}</li>
                        <li><strong>Alamat</strong></li>
                        <li>{{ $tambak->alamat }}</li>
                    </ul>

                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                <div class="prize__inner">
                    <div class="prize__thumb">
                        <img src="{{ url('storage/foto/'. $tambak->foto) }}" alt="banner images">
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>

<section class="htc__category__area ptb--100">
    <div class="container">
        <div class="htc__product__container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Ikan Pada Tambak Ini</h2>
                </div>
            </div>
            <div class="row">
                <div class="product__list clearfix mt--30">
                    <!-- Start Single Category -->
                    @forelse($ikan as $item)
                    <div class="col-md-4 col-lg-3 col-sm-4 col-xs-12">
                        <div class="category">
                            <div class="ht__cat__thumb">
                                <a href="{{route('detail-ikan',$item->id)}}">
                                    <img src="{!!url("storage/foto/".$item->foto)!!}" width="290px" height="290px" alt="product images">
                                </a>
                            </div>
                            
                            <div class="fr__product__inner">
                                <h4><a href="{{route('detail-ikan',$item->id)}}">{{$item->nama_ikan}}</a></h4>
                                <ul class="fr__pro__prize">
                                    
                                    <li>Rp. {{number_format($item->harga)}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @empty
                    <p>Belum ada ikan</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>

@endsection