@extends('template')
@section('title')
Home
@endsection

@section('link')

<style>
.banner-home {
    padding-top: 5em;
    padding-bottom: 5em;
}
.banner-home:before {
    width: 100%;
    height: 100%;
    position: absolute;
    content: "";
    background: rgba(255, 255, 255, 0.5);
    top: 0;
}
.banner-home {
    background-image: url('{{ asset("/img/banner-fish.png") }}');
    position: relative;
    background-size: cover;
}

</style>

@endsection
@section('content')
<!-- Start Slider Area -->
<div class="banner-home">

    <div class="container">
        <div class="row align-items__center">
            <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                <div class="slide">
                    <div class="slider__inner">
                        <h2>Selamat Datang</h2>
                        <h1 class="banner-text-home">Website Pemasaran dan Penjualan Hasil Tambak Ikan Mujair Di Desa Kedisan Kintamani</h1>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
       
</div>
<!-- Start Slider Area -->
<!-- Start Category Area -->
<section class="htc__category__area ptb--100">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section__title--2 text-center">
                    <h2 class="title__line">Produk Kami</h2>
                    <p>Dapatkan Ikan Terbaik Disini</p>
                </div>
            </div>
        </div>
        <div class="htc__product__container">
            <div class="row">
                <div class="product__list clearfix mt--30">
                    <!-- Start Single Category -->
                    @foreach($ikan as $item)
                    <div class="col-md-4 col-lg-3 col-sm-4 col-xs-12">
                        <div class="category">
                            <div class="ht__cat__thumb">
                                <a href="{{route('detail-ikan',$item->id)}}">
                                    <img src="{!!url("storage/foto/".$item->foto)!!}" width="290px" height="290px" alt="product images">
                                </a>
                            </div>
                            
                            <div class="fr__product__inner">
                                <h4><a href="{{route('detail-ikan',$item->id)}}">{{$item->nama_ikan}}</a></h4>
                                <ul class="fr__pro__prize">
                                    
                                    <li>Rp. {{number_format($item->harga)}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!-- End Single Category -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Category Area -->


@endsection