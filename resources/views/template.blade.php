<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Place favicon.ico in the root directory -->
    {{-- <link rel="shortcut icon" typ{{asset('e="image/x-icon" href="images/favicon.ico"> --}}
    
    

    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/plugins/jquery-confirm/css/jquery-confirm.css')}}">
    <!-- Owl Carousel min css -->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.min.css')}}">
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="{{asset('css/core.css')}}">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="{{asset('css/shortcode/shortcodes.css')}}">
    <!-- Theme main style -->
    <link rel="stylesheet" href="{{asset('style.css')}}">
    <!-- Responsive css -->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <!-- User style -->
    <link rel="stylesheet" href="{{asset('admin/plugins/parsley-js/parsley.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('admin/plugins/fontawesome-free/css/all.min.css')}}">
    <style>
        .a {
            color:#c43b68 !important;
        }
        .accordion .accordion__title:before{
            content: none;
        }
        .logo h3 {
            font-weight: bold;
            line-height: 1.4em;
        }

        h1.banner-text-home {
            font-size: 55px;
            line-height: 1.2em;
        }

        @media screen and (max-width: 610px){
            .logo h3 {
               font-size: 12px;
            }
            h1.banner-text-home {
                font-size: 30px;
                line-height: 1.2em;
            }
        }
        </style>
    @yield('link')

  
</head>

<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Body main wrapper start -->
    <div class="wrapper">
        <!-- Start Header Style -->
        <header id="htc__header" class="htc__header__area header--one">
            <!-- Start Mainmenu Area -->
            <div id="sticky-header-with-topbar" class="mainmenu__wrap sticky__header">
                <div class="container">
                    <div class="row">
                        <div class="menumenu__container clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-5"> 
                                <div class="logo">
                                    <h3><a href="{{ url('/') }}">Marketplace Ikan Mujair Desa Kedisan Kintamani</a></h3>
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-8 col-sm-5 col-xs-3">
                                <nav class="main__menu__nav hidden-xs hidden-sm">
                                    <ul class="main__menu">
                                        <li><a href="{{route('home')}}">Home</a></li>
                                        <li><a href="{{ url('ikan') }}">Ikan</a></li>
                                        <li><a href="{{ url('tambak') }}">Tambak</a></li>
                                
                                        
                                        @if(Auth::guard('pelanggan')->user())
                                        <li><a href="{{route('profil')}}"><i class="icon-user icons a"></i></a></li>
                                        <li><a href="{{route('keluar')}}" class="a">Log Out</a></li>
                                        @else
                                        <li><a href="{{route('masuk')}}" class="a">Log In</a></li>
                                        @endif
                                    </ul>
                                </nav>

                                <div class="mobile-menu clearfix visible-xs visible-sm">
                                    <nav id="mobile_dropdown">
                                        <ul>
                                            <li><a href="{{route('home')}}">Home</a></li>
                                            <li><a href="{{ url('ikan') }}">Ikan</a></li>
                                            <li><a href="{{ url('tambak') }}">Tambak</a></li>
                      
                                            
                                            @if(Auth::guard('pelanggan')->user())
                                            <li><a href="{{route('profil')}}"><i class="icon-user icons a"></i></a></li>
                                            <li><a href="{{route('keluar')}}" class="a">Log Out</a></li>
                                            @else
                                            <li><a href="{{route('masuk')}}" class="a">Log In</a></li>
                                            @endif
                                        </ul>
                                    </nav>
                                </div>  
                            </div>
                            <div class="col-md-3 col-lg-2 col-sm-4 col-xs-4">
                                <div class="header__right">
                                    
                                   
                                    <div class="htc__shopping__cart">
                                        <a href="{{ url('cart') }}"><i class="fa fa-shopping-cart"></i></a>
                                        <a href="{{ url('cart') }}"><span class="htc__qua">{{ (!empty(Session::get('cart'))) ? count(Session::get('cart')) : 0 }}</span></a>
                                    </div>

                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mobile-menu-area"></div>
                </div>
            </div>
            <!-- End Mainmenu Area -->
        </header>
        <!-- End Header Area -->

        <div class="body__overlay"></div>
        
        
        <!-- End Blog Area -->

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                    @endif
                    @if(session('error'))
                    <div class="alert alert-danger">
                        {{session('error')}}
                    </div>
                    @endif            
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>

        
        
        @yield('content')
        <!-- End Banner Area -->
        <!-- Start Footer Area -->
        <footer id="htc__footer">
            
            <!-- Start Copyright Area -->
            <div class="htc__copyright bg__cat--5">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="copyright__inner">
                                <p>Copyright © 2020. All right reserved.</p>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Copyright Area -->
        </footer>
        <!-- End Footer Style -->
    </div>
    <!-- Body main wrapper end -->

    <!-- Placed js at the end of the document so the pages load faster -->

    <!-- jquery latest version -->
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('admin/plugins/jquery-confirm/js/jquery-confirm.js')}}"></script>
    <!-- Bootstrap framework js -->
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/plugins/parsley-js/parsley.js')}}"></script>
    <!-- All js plugins included in this file. -->
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/slick.min.js')}}"></script>
    <script src="{{asset('js/owl.carousel.min.js')}}"></script>
    <!-- Waypoints.min.js. -->
    <script src="{{asset('js/waypoints.min.js')}}"></script>
    <!-- Main js file that contents all jQuery plugins activation. -->
    <script src="{{asset('js/main.js')}}"></script>
    @yield('script')
</body>

</html>