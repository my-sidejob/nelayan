@extends('admin.template')
@section('title')
Nelayan
@endsection

@section('breadcumb')
Nelayan
@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Form Admin
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form method="post"  id="form" action="{{ (!isset($user['id'])) ? route('admin.store') : route('admin.update', ['id' => $user['id']]) }}">
                                {{csrf_field()}}

                                @isset($user['id'])
                                    {{ method_field('PUT')}}
                                @endisset
                                <b>Data User</b>
                                <hr>
                                
                                <div class="form-group">
                                    <label for="">Nama</label>
                                    <input type="text" class="form-control col-md-6 input-value" id="Nama" name="nama" required value=" {{ old('nama', $user['nama']) }} ">
                                </div>
                               
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" class="form-control col-md-6 input-value" id="Email" name="email" required value=" {{ old('email', $user['email']) }} ">
                                </div>
                                <div class="form-group">
                                    <label for="">Username</label>
                                    <input type="text" class="form-control col-md-6 input-value" id="Username" name="username" required value=" {{ old('username', $user['username']) }} ">
                                </div>
                                @if(!isset($user['id']))                                
                                <div class="form-group form-password">
                                    <label for="">Password</label>
                                    <input type="password" class="form-control col-md-6 input-value" id="Password" name="password">
                                </div>
                                <div class="form-group form-password">
                                    <label for="">Password (lagi)</label>
                                    <input type="password" class="form-control col-md-6 input-value" id="Password" name="password_confirmation">
                                </div>
                                @endif
                                <input type="hidden" name="level" value="1">
                                <div class="form-group">
                                    <input type="submit" value="Simpan" class="btn btn-primary float-right">
                                </div>
                                        
                            </form>
                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')

    
@endsection
