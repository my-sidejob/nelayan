@extends('admin.template')
@section('title')
Admin
@endsection

@section('breadcumb')
Admin
@endsection


@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Data Admin
                    </div>
                    <div class="card-tools">
                        <a  href="{{route('admin.create')}}" class="btn btn-primary btn-tambah">Tambah Data <i class="fa fa-plus"></i></a>
                    </div>
                </div>
                <div class="card-body">
                    <br>
                    <div>
                      <table id="Table" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <!-- <th>Foto</th> -->
                            <th>No</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($admin as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama }}</td>
                                <td>{{ $row->username }}</td>
                                <td>{{ $row->email }}</td>
                                <td>Admin</td>
                                <td>
                                    <form action=" {{ route('admin.destroy', ['id' => $row->id]) }} " method="post">
                                        @csrf
                                        @method('delete')
                                            <a href="{{ route('admin.edit', ['id' => $row->id]) }}" class="btn btn-warning"><i class="fa fa-cog"></i></a>
                                            <button type="submit" class="btn btn-danger btn-delete" onclick="return confirm('Yakin akan hapus data?')"><i class="fa fa-trash text-white"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                       
                      </table>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script>

    $(document).ready(function(){
        $('.liadmin').addClass('active');
      
       @if(session()->has('success'))
            toastr.success("{{session('success')}}")
            
       @endif
       
    })

</script>

@endsection