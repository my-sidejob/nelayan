@extends('admin.template')
@section('title')
Dashboard
@endsection

@section('breadcumb')
Dashboard
@endsection

@section('content')
<div class="container-fluid">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box">
          <span class="info-box-icon bg-info elevation-1"><i class="fas fa-box"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Jumlah Produk</span>
            <span class="info-box-number">
              {{ $total_produk }}
            </span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
          <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-thumbs-up"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Jumlah Tambak</span>
            <span class="info-box-number">{{ $jumlah_tambak }}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->

      <!-- fix for small devices only -->
      <div class="clearfix hidden-md-up"></div>

      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
          <span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Jumlah Transaksi</span>
            <span class="info-box-number">{{ $jumlah_transaksi }}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-12 col-sm-6 col-md-3">
        <div class="info-box mb-3">
          <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-dollar-sign"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Omset</span>
            <span class="info-box-number">{{ number_format($jumlah_omset) }}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            Selamat Datang
          </div>
          <div class="card-body">
            Marketplace Ikan Mujair Desa Kedisan Kintamani
          </div>
        </div>
      </div>
    </div>

   
  </div><!--/. container-fluid -->
@endsection

@section('js')
<script>
   $(document).ready(function(){
        $('.lidashboard').addClass('active');
       
    })
</script>
@endsection