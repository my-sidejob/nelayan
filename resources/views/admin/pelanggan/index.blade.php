@extends('admin.template')
@section('title')
Pelanggan
@endsection

@section('breadcumb')
Tambak
@endsection

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Data Pelanggan
                    </div>                 
                </div>
                <div class="card-body">
                    <br>
                    <div>
                      <table id="Table" class="table table-bordered table-hover">
                        <thead>
                          <tr>
                              <th>No</th>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>No Telp</th>
                            <th>Alamat</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach($pelanggan as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama }}</td>
                                <td> {{ $row->username }} </td>
                                <td> {{ $row->email }} </td>
                                <td>{{ $row->no_telp }}</td>
                                <td> {{ $row->alamat }} </td>
                                <td> 
                                    <form action=" {{ route('pelanggan.destroy', ['id' => $row->id]) }} " method="post">
                                    @csrf
                                    @method('delete')
                                        
                                        <button type="submit" class="btn btn-danger btn-delete" onclick="return confirm('Yakin akan hapus data?')"><i class="fa fa-trash text-white"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                       
                      </table>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')

<script>

$(document).ready(function(){
        $('.lipelanggan').addClass('active');
      
       @if(session()->has('success'))
            toastr.success("{{session('success')}}")
            
       @endif
       
    })

</script>

@endsection