@extends('admin.template')
@section('title')
Data Transaksi
@endsection

@section('breadcumb')
Transaksi
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Daftar Pembelian                  
                    </div>                    
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No. Invoice</th>
                                <th>Tambak</th>
                                <th>Tanggal</th>
                                <th>Total</th>
                                <th>Status</th>
                                <th>Pelanggan</th>
                                <th>Detail</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pembelian as $row)
                            @if(Auth::user()->level != 1) 
                                @if($row->checkTambakOwner() != Auth::user()->id)
                                    <?php continue ?>
                                @endif
                            @endif
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->invoice }}</td>
                                <td>{{ $row->checkTambak() }}</td>
                                <td>{{ $row->tanggal_pembelian }}</td>
                                <td>{{ number_format($row->total_nominal) }}</td>
                                <td>{{ $row->getStatusPembelian() }}</td>
                                <td>{{ $row->pelanggan->nama }}</td>
                                <td><a href="{{ route('transaksi.detail', ['transaksi_id' => $row->id]) }}" class="btn btn-sm btn-info">Detail</a></td>
                            </tr>
                            @endforeach                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script>

    $(document).ready(function(){
        $('.litransaksi').addClass('active');
      
       @if(session()->has('success'))
            toastr.success("{{session('success')}}")
            
       @endif
       
    })
</script>
@endsection