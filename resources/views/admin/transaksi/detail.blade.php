@extends('admin.template')
@section('title')
Detail Transaksi
@endsection

@section('breadcumb')
Detail Transaksi
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Transaksi
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <td>No Invoice</td>
                            <td>:</td>
                            <td>{{ $pembelian->invoice }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>:</td>
                            <td>{{ $pembelian->created_at }}</td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td>:</td>
                            <td>{{ number_format($pembelian->total_nominal) }}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>{{ $pembelian->getStatusPembelian() }}</td>
                        </tr>
                        <tr>
                            <td>Pelanggan</td>
                            <td>:</td>
                            <td>{{ $pembelian->pelanggan->nama }}</td>
                        </tr>
                        
                    </table>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Status Pembelian
                </div>
                <div class="card-body">
                    <form action="{{ route('transaksi.change-status', ['transaksi_id' => $pembelian->id]) }}" method="post">
                        @csrf 
                        @method('put')
                        <select name="status_pembelian" class="form-control">
                            <option value="1" {{ ($pembelian->status_pembelian == 1) ? 'selected' : '' }} >Menunggu Pembayaran</option>
                            <option value="2" {{ ($pembelian->status_pembelian == 2) ? 'selected' : '' }} >Menunggu Konfirmasi Pembayaran</option>
                            <option value="3" {{ ($pembelian->status_pembelian == 3) ? 'selected' : '' }} >Sedang Dikirim</option>
                            <option value="4" {{ ($pembelian->status_pembelian == 4) ? 'selected' : '' }} >Selesai</option>
                            <option value="5" {{ ($pembelian->status_pembelian == 5) ? 'selected' : '' }} >Batal</option>
                        </select>
                        <br>
                        <input type="submit" value="Save" class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            

            <div class="card">
                
                <div class="card-header">
                    Bukti Pembayaran
                </div>
                @if($pembelian->pembayaran != "")
                                                    
                <div class="card-body">
                    <a target="_blank" href="{{ url('storage/foto/'. $pembelian->pembayaran->bukti_pembayaran) }}"><img src="{{ url('storage/foto/'. $pembelian->pembayaran->bukti_pembayaran) }}" class="img-fluid"></a>
                </div>
                    
                @else 
                <div class="card-body">
                    Belum ada bukti pembayaran
                </div>
                @endif
            </div>
        </div>

        
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Detail Item {{ $pembelian->invoice }}
                    </div>                    
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Item</th>
                                <td>Qty</td>
                                <th>Harga</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pembelian->detailPembelian as $row)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->ikan->kode }}</td>
                                <td>{{ $row->ikan->nama_ikan }}</td>
                                <td>{{ $row->qty }}</td>
                                <td>{{ number_format($row->ikan->harga) }}</td>        
                                
                            </tr>
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Ongkir ke {{ $pembelian->pengiriman->kabupaten }}</td>
                                <td>:</td>
                                <td>{{ number_format($pembelian->pengiriman->ongkir) }}</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><strong>Total</strong></td>
                                <td>:</td>
                                <td>{{ number_format($pembelian->total_nominal) }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

<script>

    $(document).ready(function(){
        $('.litransaksi').addClass('active');
      
       @if(session()->has('success'))
            toastr.success("{{session('success')}}")
            
       @endif
       
    })
</script>
@endsection